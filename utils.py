import numpy as np
import cv2
import dsift as dsift
import os
import re
from scipy.cluster.vq import vq, whiten, kmeans2
from sklearn.cluster import KMeans
import time as t
from vocabulary import vocabulary
from rootsift import RootSIFT

def validate_top_result(probe_img_name, result_img_name, mode = 1):
	""" 
		Validates if the lowest distance result is Good or not. Assumes a sorted list for result_img_name.
		Probe Image Number is always higher than Test Image Number for same label.
	"""
	probe_img_number = extract_image_number(probe_img_name)
	result_img_number = extract_image_number(result_img_name)

	if (mode == 0):
		if (probe_img_number - result_img_number) >= 0:
			if (probe_img_number - result_img_number) < 4:
				return True
		return False
	elif mode == 1:
		if (probe_img_number == result_img_number):
			return True
		return False
	else:
		if(probe_img_number - result_img_number) >= -3:
			if (probe_img_number - result_img_number) <= 0:
				return True
		return False

def match_in_top_10(probe_img_name, result_img_names, mode = 0):
	"""
		Finds a match (does not count) and returns true if one exists
		in the top 10 results
	"""
	for a_match in result_img_names[:5]:
		if validate_top_result(probe_img_name, a_match[1]):
			return True

	return False

def get_matches_in_list(probe_img_name, result_img_names, top = 10, mode = 0):
	match_count = 0.0
	for a_match in result_img_names[:top]:
		if validate_top_result(probe_img_name, a_match[1]):
			match_count += 1.0
	return match_count


def extract_image_number(filename, digit_length = 3):
	""" 
		Extracts the image number from filenames like
		1. IMG_2354.jpg
		2. ukbench10199.jpg

		Requires a 3 letter extension.
	"""

	number = int(filename[len(filename)-(digit_length+4):len(filename)-4])
	return number

def get_images(target_dir = 'training_imgs/', compute_descriptors = None):
	file_local_descriptors = {}
	filename = None
	file_count = 0.0
	total_descriptors = 0
	for afile in os.listdir(target_dir):
		if re.match('.*\.jpg|.*\.png$', afile):
			filename = target_dir + afile
			file_count += 1.0
			if compute_descriptors is None:
				file_local_descriptors.update({filename : None})
			else:
				img = cv2.imread(filename, 0)
				local_descriptors = compute_descriptors(img)
				file_local_descriptors.update({filename : local_descriptors})
				total_descriptors += local_descriptors.shape[0]
				#print "Features found in %s: %d" % (afile, local_descriptors.shape[0])
	#print "----------"
	#print "Total Features Detected: %d" % total_descriptors
	print file_count	
	print "Average number of features: %.2f" % (total_descriptors/file_count)
	return file_local_descriptors, total_descriptors


def get_descriptors_dsift(image):
	### Returns a Normalized Decriptor ###
	grid = 16
	patch = 32
	extractor = dsift.DsiftExtractor(grid, patch)
	descriptors, _ = extractor.process_image(image)
	return descriptors

def get_descriptors_sift(image, sigma = 1.6):
	extractor = cv2.xfeatures2d.SIFT_create(sigma = 1.0)
	_, descriptors = extractor.detectAndCompute(image, None)
	return descriptors

def get_descriptors_surf(image):
	extractor = cv2.xfeatures2d.SURF_create()
	_, descriptors = extractor.detectAndCompute(image, None)
	return descriptors

def get_descriptors_msersift(image):
	detector = cv2.MSER_create()
	extractor = cv2.xfeatures2d.SIFT_create()

	kp = detector.detect(image)
	_, descriptors = extractor.compute(image, kp)
	return descriptors

def get_descriptors_rootsift(image):
	detector = cv2.xfeatures2d.SIFT_create()
	kps = detector.detect(image)

	rs = RootSIFT()
	(kps, descriptors) = rs.compute(image, kps)
	return descriptors


def write_descriptors_to_file(descriptors, fname):
	filename = open(fname, "w")
	for a_descriptor in descriptors:
		filename.write(np.array_str(a_descriptor, max_line_width = 5))
	
	filename.close()

def __main__():

	file_local_descriptors, descriptor_count = get_images(compute_descriptors = get_descriptors_sift)
	K = 1000
	voc = vocabulary()

	try:
		codebook = np.load('codebook-%d.npy' % K)
		voc.voc = codebook
		voc.vocabulary_size = codebook.shape[0]
	except IOError:
		voc.train(file_local_descriptors, K)

	img = cv2.imread('test_imgs/ukbench00057.jpg',0)
	descriptor = get_descriptors_sift(img)
	img_hist = voc.project(descriptor)
	bow = voc.project_training_set(file_local_descriptors)
	voc.search(img_hist)

	#voc.plot_all_histograms(img_hist)