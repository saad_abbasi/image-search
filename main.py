import numpy as np
import cv2
import dsift as dsift
import os
import re
from scipy.cluster.vq import vq, whiten, kmeans2
from sklearn.cluster import KMeans
import time as t
from vocabulary import vocabulary

def get_images(target_dir = 'training_imgs/', compute_descriptors = None):
	file_local_descriptors = {}
	filename = None
	total_descriptors = 0
	for afile in os.listdir(target_dir):
		if re.match('.*\.jpg|.*\.png$', afile):
			filename = target_dir + afile
			if compute_descriptors is None:
				file_local_descriptors.update({filename : None})
			else:
				img = cv2.imread(filename, 0)
				local_descriptors = compute_descriptors(img)
				file_local_descriptors.update({filename : local_descriptors})
				total_descriptors += local_descriptors.shape[0]
				print "Features found in %s: %d" % (afile, local_descriptors.shape[0])
	print "----------"
	print "Total Features Detected: %d" % total_descriptors	
	return file_local_descriptors, total_descriptors


def get_descriptors_dsift(image):
	### Returns a Normalized Decriptor ###
	grid = 4
	patch = 8
	extractor = dsift.DsiftExtractor(grid, patch)
	descriptors, _ = extractor.process_image(image)
	return descriptors

def get_descriptors_sift(image):
	extractor = cv2.xfeatures2d.SIFT_create()
	_, descriptors = extractor.detectAndCompute(image, None)
	return descriptors

def write_descriptors_to_file(descriptors, fname):
	filename = open(fname, "w")
	for a_descriptor in descriptors:
		filename.write(np.array_str(a_descriptor, max_line_width = 5))
	
	filename.close()

# def __main__():
# 	file_local_descriptors, descriptor_count = get_images(compute_descriptors = get_descriptors_sift)
# 	K = 1000
# 	voc = vocabulary()

# 	try:
# 		codebook = np.load('codebook-%d.npy' % K)
# 		voc.voc = codebook
# 		voc.vocabulary_size = codebook.shape[0]
# 	except IOError:
# 		voc.train(file_local_descriptors, K)

# 	img = cv2.imread('test_imgs/ukbench00057.jpg',0)
# 	descriptor = get_descriptors_sift(img)
# 	img_hist = voc.project(descriptor)
# 	bow = voc.project_training_set(file_local_descriptors)
# 	voc.search(img_hist)

# 	#voc.plot_all_histograms(img_hist)