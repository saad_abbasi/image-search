import numpy
import unittest
import utils

class TestUtils(unittest.TestCase):
	def test_extract_image_number(self):
		self.assertEqual(utils.extract_image_number('IMG_2354.jpg', digit_length = 4),2354)
		self.assertEqual(utils.extract_image_number('ukbench10199.jpg'),10199)


	def test_does_the_top_search_result_match(self):
		self.assertTrue(utils.validate_top_result('ukbench10199.jpg', 'ukbench10199.jpg'))
		self.assertTrue(utils.validate_top_result('ukbench10200.jpg', 'ukbench10199.jpg'))
		self.assertTrue(utils.validate_top_result('ukbench10201.jpg', 'ukbench10199.jpg'))
		self.assertTrue(utils.validate_top_result('ukbench10202.jpg', 'ukbench10199.jpg'))
		self.assertFalse(utils.validate_top_result('ukbench10203.jpg', 'ukbench10199.jpg'))
		self.assertFalse(utils.validate_top_result('ukbench10198.jpg', 'ukbench10199.jpg'))

		self.assertFalse(utils.validate_top_result('ukbench10188.jpg', 'ukbench10192.jpg', mode = 2))
		
	def test_match_in_top_10(self):
		self.assertTrue(utils.match_in_top_10('ukbench10204.jpg',[(100,'ukbench10203.jpg'),(200,'ukbench10199.jpg')]))
		#self.assertTrue('ukbench10199.jpg',[(100,'ukbench10204.jpg'),(200,'ukbench10205.jpg')])

	def test_get_matches_in_list(self):
		self.assertEqual(utils.get_matches_in_list('ukbench10204.jpg',[(100,'ukbench10203.jpg'),(200,'ukbench10199.jpg')]),1)
		self.assertEqual(utils.get_matches_in_list('ukbench10204.jpg',[(100,'ukbench10203.jpg'),(200,'ukbench10202.jpg')]),2)
		self.assertEqual(utils.get_matches_in_list('ukbench10204.jpg',[(100,'ukbench10303.jpg'),(200,'ukbench10502.jpg')]),0)

if __name__ == '__main__':
	unittest.main()