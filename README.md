### What is this repository for? ###

This is an experimental Content Image Retreival System designed to identify medicines from their images. The idea was to use medicine packaging imagery in lieu of barcodes. 
I use the python library Visual Logging to log the results to an HTML file. Here is a sample. The left-most image is the query image and the 2nd, 3rd and 4th image
from the left are the query results.

On 100 query images, if we count only the 1st result, I achieved an accuracy of 95%.

The program works on by computing a Bag of Visual Words based on D-SIFT descriptors. The histograms are saved in a database (in the case of this experiment, in a Pickle file)
When querying, an image's histrogram is computed the same way and then compared against the pickle file using CHI-SQUARE. I tried using Euclidean distance to compare the
histograms but the results were mich better with CHI-SQUARE.

![Alt text](https://i.imgur.com/j4v5dRt.jpg)

### How do I get set up? ###

You will need Python 2.7, OpenCV, VisualLogging and numpy to run this project.