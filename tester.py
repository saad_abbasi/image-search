import numpy as np
import utils
import re
import os
import cv2
from vocabulary import vocabulary
import pickle

TARGET_DIR = 'test_imgs/'

def extract_image_number(filename):
	number = int(filename[len(filename)-8:len(filename)-4])
	return number


try:
	training_img_descriptors = pickle.load(open("training_img_descriptors.p","rb"))
except IOError:
	training_img_descriptors, _ = utils.get_images(compute_descriptors = utils.get_descriptors_rootsift)
	pickle.dump(training_img_descriptors, open("training_img_descriptors.p","wb"))

try:
	probe_img_descriptors = pickle.load(open("bow_probe.p","rb"))
except IOError:
	probe_img_descriptors, _ = utils.get_images(target_dir = TARGET_DIR, compute_descriptors = utils.get_descriptors_rootsift)
	pickle.dump(probe_img_descriptors, open("bow_probe.p","wb"))

#K = 125
k = [10000]

for K in k:
	voc = vocabulary()
	BOW_FILE_NAME = 'bow-%d.p' % K
	try:
		codebook = np.load('codebook-%d.npy' % K)
		voc.voc = codebook
		voc.vocabulary_size = codebook.shape[0]
	except IOError:
		voc.train(training_img_descriptors, K)

	try:
		bow = pickle.load(open(BOW_FILE_NAME,"rb"))
		voc.bow = bow
	except IOError:
		bow = voc.project_training_set(training_img_descriptors)
		pickle.dump(bow, open(BOW_FILE_NAME,"wb"))


	#print "----------- TEST RESULTS ----------------"
	match_count = {}
	top_match_count = 0.0
	top_ten_match_count = 0.0
	match_quality = 0.0
	for filename, descriptor in probe_img_descriptors.items():
		match_count.update({filename : 0})
		hist = voc.project(descriptor)
		result = voc.search(hist, top = 10)
		if utils.validate_top_result(filename, result[0][1]):
			top_match_count += 1
		if utils.match_in_top_10(filename, result):
			top_ten_match_count += 1
		match_quality += utils.get_matches_in_list(filename, result, top = 3)


	#print "%.2f\t\t%.2f\t\#t%d" % (top_ten_match_count/len(probe_img_descriptors),
							#		top_match_count/len(probe_img_descriptors), K)

	print "{:.2f}\t\t{:.2f}\t\t{:.2f}\t\t{}".format(
												top_ten_match_count/len(probe_img_descriptors),
												top_match_count/len(probe_img_descriptors),
												match_quality/len(probe_img_descriptors),
													K)

